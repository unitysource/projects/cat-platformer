﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseGame : MonoBehaviour
{
    Animator an;

    public GameObject PauseMenu;
    public GameObject PauseBoard;

    private void Start()
    {
        an = PauseBoard.GetComponent<Animator>();
    }
    public void Pause()
    {
        StartCoroutine(AppearPauseMenu());
        PauseMenu.SetActive(true);
    }

    public void Play()
    {
        Time.timeScale = 1;
        an.SetTrigger("Disappeared");
        StartCoroutine(SetActive());
    }

    public void Restart()
    {
        Time.timeScale = 1;
        an.SetTrigger("Disappeared");
        StartCoroutine(RestartScene());
    }

    public void Home()
    {
        Time.timeScale = 1;
        an.SetTrigger("Disappeared");
        StartCoroutine(ToMenu());
    }

    IEnumerator AppearPauseMenu()
    {
        yield return new WaitForSeconds(1f);
        Time.timeScale = 0;
    }

    IEnumerator SetActive()
    {
        yield return new WaitForSeconds(.9f);
        PauseMenu.SetActive(false);
    }

    IEnumerator RestartScene()
    {
        yield return new WaitForSeconds(.8f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    IEnumerator ToMenu()
    {
        yield return new WaitForSeconds(.8f);
        SceneManager.LoadScene("Menu");
    }
}
