﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorDown : MonoBehaviour
{
    [SerializeField]
    float speed;
    bool bool1;
    void Start()
    {

    }

    void Update()
    {
        if (bool1)
            transform.position += Vector3.down * speed * Time.deltaTime;
        else
            transform.position = new Vector2(transform.position.x, transform.position.y);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "player")
        {
            bool1 = true;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "player")
        {
            bool1 = false;
        }
    }
}
