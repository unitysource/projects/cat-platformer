﻿using System.Collections;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    public Camera mainCamera;
    public Button lvl2Btn, lvl3Btn;
    public GameObject panepClearBlack, panepClearBlackInStartMenu, loadingMan, LevelsPaper, levelsWindow;
    bool panelPassed;
    bool startMenuBool;
    public Image LoadingImg;
    public Text progressText;
    int levelPassed;
    static int musicPassed, soundsPassed;

    [Header("Settings")]
    [SerializeField]
    private GameObject settingsWindow;
    [SerializeField]
    private GameObject settingsPanel;
    [SerializeField]
    private Toggle soundsToggle;
    [SerializeField]
    private Toggle musicToggle;

    void Start()
    {

        levelPassed = PlayerPrefs.GetInt("LevelPassed");
        musicPassed = PlayerPrefs.GetInt("musicPassed");
        soundsPassed = PlayerPrefs.GetInt("soundsPassed");
        lvl2Btn.interactable = false;
        lvl3Btn.interactable = false;

        switch (levelPassed)
        {
            case 1:
                lvl2Btn.interactable = true;
                break;
            case 2:
                lvl2Btn.interactable = true;
                lvl3Btn.interactable = true;
                break;
        }
        mainCamera.orthographicSize = 16;
        startMenuBool = true;

        if (musicPassed == 1)
        {
            musicToggle.isOn = true;
        }
        else if (musicPassed == 0)
        {
            musicToggle.isOn = false;
        }
        if (soundsPassed == 1)
            soundsToggle.isOn = true;
        else
            soundsToggle.isOn = false;
        if(musicToggle.isOn)
            GetComponent<AudioSource>().volume = 1;
        else if (!musicToggle.isOn)
            GetComponent<AudioSource>().volume = 0;

    }

    private void Update()
    {
        if (mainCamera.orthographicSize > 6)
            ChangeSizeCamera();
        else if (mainCamera.orthographicSize < 6)
            mainCamera.orthographicSize = 6;

    }

    private void FixedUpdate()
    {
        if (panelPassed)
            StartCoroutine(PanelBlackAndLoading());
        if (startMenuBool)
            StartCoroutine(StartMenu());
    }

    public void LevelToLoad(int level)
    {
        panelPassed = true;
        LevelsPaper.GetComponent<Animator>().SetTrigger("Trigger");
        StartCoroutine(LoadingAsyncFromMenu(level));

        //StartCoroutine(loadScene(level));
    }
    public void LevelsWindowOff()
    {
        LevelsPaper.GetComponent<Animator>().SetTrigger("Trigger");
        StartCoroutine(LevelsWindowOffCar());
    }
    public void SettingsWindowOn() => settingsWindow.SetActive(true);

    public void SettingsWindowOff()
    {
        settingsPanel.GetComponent<Animator>().SetTrigger("Trigger");
        StartCoroutine(SettingsWindowOffCar());
    }

    public void MusicChangeToggle()
    {
        if (musicPassed == 0)
            PlayerPrefs.SetInt("musicPassed", 1);
        else
            PlayerPrefs.SetInt("musicPassed", 0);

        if (musicToggle.isOn)
            GetComponent<AudioSource>().volume = 1;
        else
            GetComponent<AudioSource>().volume = 0;
    }

    public void SoundsChangeToggle()
    {
        if (soundsPassed == 0)
            PlayerPrefs.SetInt("soundsPassed", 1);
        else
            PlayerPrefs.SetInt("soundsPassed", 0);
    }

    public void YouTubeSettings() => Application.OpenURL("https://www.youtube.com/watch?v=hH-8aeyBcXo");
    public void InstSettings() => Application.OpenURL("https://www.instagram.com/mirskiy_paul/");
    void ChangeSizeCamera()
    {
        mainCamera.GetComponent<Camera>().orthographicSize -= 5 * Time.deltaTime;
    }
    IEnumerator LevelsWindowOffCar()
    {
        yield return new WaitForSeconds(1.5f);
        levelsWindow.SetActive(false);

    }
    IEnumerator SettingsWindowOffCar()
    {
        yield return new WaitForSeconds(1.5f);
        settingsWindow.SetActive(false);

    }
    IEnumerator StartMenu()
    {
        panepClearBlackInStartMenu.GetComponent<Image>().color = new Color(panepClearBlackInStartMenu.GetComponent<Image>().color.r - .01f * Time.deltaTime,
            panepClearBlackInStartMenu.GetComponent<Image>().color.g - .01f * Time.deltaTime, panepClearBlackInStartMenu.GetComponent<Image>().color.b - .01f * Time.deltaTime,
            panepClearBlackInStartMenu.GetComponent<Image>().color.a - .5f * Time.deltaTime);
        yield return new WaitForSeconds(1.8f);
        panepClearBlackInStartMenu.SetActive(false);
        startMenuBool = false;
    }

    IEnumerator PanelBlackAndLoading()
    {
        panepClearBlack.SetActive(true);
        panepClearBlack.GetComponent<Image>().color = new Color(panepClearBlack.GetComponent<Image>().color.r + .01f * Time.deltaTime,
            panepClearBlack.GetComponent<Image>().color.g + .01f * Time.deltaTime, panepClearBlack.GetComponent<Image>().color.b + .01f * Time.deltaTime,
            panepClearBlack.GetComponent<Image>().color.a + .5f * Time.deltaTime);
        yield return new WaitForSeconds(1.8f);
        panelPassed = false;
        loadingMan.SetActive(true);
    }

    //IEnumerator loadScene(int level)
    //{
    //    yield return new WaitForSeconds(2f);
    //    SceneManager.LoadScene(level);
    //}
    IEnumerator LoadingAsyncFromMenu(int level)
    {
        yield return new WaitForSeconds(1.8f);
        AsyncOperation operation = SceneManager.LoadSceneAsync(level);
        while (!operation.isDone)
        {
            float prog = operation.progress / 0.9f;
            LoadingImg.fillAmount = prog;
            progressText.text = string.Format("{0:0}%", prog * 100);
            yield return null;
        }
    }


    public void ResetPlayerPlefs()
    {
        PlayerPrefs.DeleteAll();
        lvl2Btn.interactable = false;
        lvl3Btn.interactable = false;
    }
}
