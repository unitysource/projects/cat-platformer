﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UnlockLevels : MonoBehaviour
{
    int levelPassed, indexCurScene;
    void Start()
    {
        levelPassed = PlayerPrefs.GetInt("LevelPassed");

        indexCurScene = SceneManager.GetActiveScene().buildIndex - 2;
    }

    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D c)
    {
       if(c.gameObject.tag == "finish")
        {
            if (SceneManager.GetActiveScene().name == "Level_3") { }
            else
            {
                if (levelPassed < indexCurScene)
                {
                    PlayerPrefs.SetInt("LevelPassed", indexCurScene);
                }
            }
        }
    }

}
