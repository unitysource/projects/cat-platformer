﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    Animator an;
    Rigidbody2D riba;
    AudioSource audS;
    public GameObject btnL;
    public GameObject btnR;
    public GameObject btnU;
    public GameObject panepClearBlack;
    public GameObject LoadingBG;
    public GameObject GameHelp;

    public AudioClip landAu;
    public AudioClip JumpAu;
    public AudioClip deathAu;
    public AudioClip respawnAu;
    public AudioClip RunAu;
    public AudioClip EnemyAu;
    public AudioClip DownAu;
    public AudioClip WaterAu;
    public AudioClip BottleWaterAu;

    float volumeSounds = .5f;
    float posBtnL;
    float posBtnR;
    float posBtnU;
    float run;
    public float runSpeed = 4;
    public float jumpForce = 4;
    bool whenlook;
    bool isGrounded = true;
    bool canBtn;
    bool stopAudioRun;
    public bool startL;
    public bool victory;
    public Transform groundCheck;
    public Transform spawnP;
    public LayerMask whatIsGround;
    int sceneInd;

    private void Awake()
    {
        Time.timeScale = 1f;
        riba = GetComponent<Rigidbody2D>();
        an = GetComponent<Animator>();
        audS = GetComponent<AudioSource>();
    }

    void Start()
    {
        posBtnL = btnL.transform.position.y;
        posBtnR = btnR.transform.position.y;
        posBtnU = btnU.transform.position.y;
        whenlook = false;
        transform.position = spawnP.position;
        canBtn = true;
        victory = false;
        startL = true;
        panepClearBlack.SetActive(true);

        Scene scene = SceneManager.GetActiveScene();
        sceneInd = scene.buildIndex;
    }

    private void FixedUpdate()
    {
        CheckGround();

        if (victory)
            StartCoroutine(FinishLevel());
        if (startL)
            StartCoroutine(StartLevel());
    }

    void Update()
    {
        if (canBtn)
        {
            if (posBtnU != btnU.transform.position.y && isGrounded)
            {
                //riba.AddForce(transform.up * jumpForce, ForceMode2D.Impulse);
                riba.velocity = Vector2.up * jumpForce;
                an.SetBool("IsGrounded", false);
                audS.PlayOneShot(JumpAu, volumeSounds);
            }
            else if (isGrounded)
            {
                an.SetBool("IsGrounded", true);
                //an.SetFloat("velocityX", 0);
            }

            if (posBtnL != btnL.transform.position.y)
            {
                run = -runSpeed;
                whenlook = false;
                Flip();
                an.SetFloat("velocityX", Mathf.Abs(riba.velocity.x) / runSpeed);
                if (!stopAudioRun & isGrounded)
                    StartCoroutine(StopAudioRun());

            }
            else if (posBtnR != btnR.transform.position.y)
            {
                run = runSpeed;
                whenlook = true;
                Flip();
                an.SetFloat("velocityX", Mathf.Abs(riba.velocity.x) / runSpeed);
                if (!stopAudioRun & isGrounded)
                    StartCoroutine(StopAudioRun());
            }
            else
            {
                run = 0;
                an.SetFloat("velocityX", 0);
            }

            riba.velocity = new Vector2(run, riba.velocity.y);
            if (victory || startL)
            {
                riba.velocity = new Vector2(runSpeed, riba.velocity.y);
                an.SetFloat("velocityX", Mathf.Abs(riba.velocity.x) / runSpeed);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D c)
    {
        if (c.gameObject.tag == "finish")
        {
            an.SetBool("Victory", true);
            victory = true;
            GameHelp.GetComponent<BottleSystem>().enabled = false;
            //canBtn = false;

            Invoke("Victory", 3f);
        }
        if(c.gameObject.tag == "floor")
        {
            audS.PlayOneShot(DownAu, volumeSounds);

            Invoke("RestartScene", DownAu.length);
        }
        if (c.gameObject.tag == "waterDead")
        {
            audS.PlayOneShot(WaterAu, volumeSounds);

            Invoke("RestartScene", WaterAu.length);
        }
        if (c.gameObject.tag == "water")
        {
            audS.PlayOneShot(BottleWaterAu, volumeSounds);
            Destroy(c.gameObject, .1f);
            GameObject[] bottles = GameObject.FindGameObjectsWithTag("bottle");
            foreach (GameObject itm in bottles)
            {
                itm.GetComponent<Image>().color = Color.white;
            }
            //GameObject b = GameObject.FindGameObjectWithTag("bottle");
            //b.GetComponent<Image>().color = Color.white;
        }
        if (c.gameObject.tag == "speedUp")
        {
            audS.PlayOneShot(BottleWaterAu, volumeSounds);
            runSpeed = 8f;
            StartCoroutine(FastTimeOff());
            Destroy(c.gameObject, .1f);
        }
        //if (c.gameObject.tag == "enemy")
        //{
        //    canBtn = false;
        //    an.SetBool("Dead", true);
        //    Invoke("Spawn", .8f);
        //}
        //if(c.gameObject.tag == "wall")
        //{
        //    //canBtn = false;
        //    GetComponent<BoxCollider2D>().enabled = false;
        //}
    }
    private void OnCollisionEnter2D(Collision2D c)
    {
        if(c.gameObject.tag == "enemy")
        {
            PlayerEventController.inst.TakeDamage();
            audS.PlayOneShot(EnemyAu);
            canBtn = false;
            an.SetBool("Dead", true);
            Invoke("Spawn", .8f);
        }
        if (c.gameObject.layer == 9)
            audS.PlayOneShot(landAu, volumeSounds);
        if (c.gameObject.layer == 9 & !isGrounded)
            StartCoroutine(ButtonsOffForSeconds());
    }

    void Flip()
    {
        if(whenlook == false)
        {
            transform.localRotation = Quaternion.Euler(0, 180, 0);
        }
        if (whenlook == true)
        {
            transform.localRotation = Quaternion.Euler(0, 0, 0);
        }
    }

    private void CheckGround()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, .3f, whatIsGround);

        if (!isGrounded) return;
    }

    void RestartScene() => SceneManager.LoadScene(sceneInd);

    void Spawn()
    {
        SceneManager.LoadScene(sceneInd);
        //transform.position = spawnP.position;
        an.SetBool("Dead", false);
    }

    void Victory()
    {
        an.SetBool("Victory", false);
        victory = false;
        //GameHelp.GetComponent<BottleSystem>().enabled = true;
        LoadingBG.SetActive(true);
    }
    IEnumerator FastTimeOff()
    {
        yield return new WaitForSeconds(5f);
        runSpeed = 6f;
    }
    IEnumerator ButtonsOffForSeconds()
    {
        canBtn = false;
        yield return new WaitForSeconds(.6f);
        canBtn = true;
    }


    IEnumerator FinishLevel()
    {
        panepClearBlack.SetActive(true);
        yield return new WaitForFixedUpdate();
        panepClearBlack.GetComponent<Image>().color = new Color(panepClearBlack.GetComponent<Image>().color.r + .01f * Time.deltaTime,
            panepClearBlack.GetComponent<Image>().color.g + .01f * Time.deltaTime, panepClearBlack.GetComponent<Image>().color.b + .01f * Time.deltaTime,
            panepClearBlack.GetComponent<Image>().color.a + .5f * Time.deltaTime);
    }

    IEnumerator StartLevel()
    {
        panepClearBlack.GetComponent<Image>().color = new Color(panepClearBlack.GetComponent<Image>().color.r - .01f * Time.deltaTime,
            panepClearBlack.GetComponent<Image>().color.g - .01f * Time.deltaTime, panepClearBlack.GetComponent<Image>().color.b - .01f * Time.deltaTime,
            panepClearBlack.GetComponent<Image>().color.a - .5f * Time.deltaTime);
        yield return new WaitForSeconds(1.8f);
        panepClearBlack.SetActive(false);
        startL = false;
    }

    IEnumerator StopAudioRun()
    {
        stopAudioRun = true;
        audS.PlayOneShot(RunAu, volumeSounds);
        yield return new WaitForSeconds(RunAu.length);
        stopAudioRun = false;
    }
}








