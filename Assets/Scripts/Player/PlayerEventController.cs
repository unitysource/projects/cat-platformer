﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEventController: MonoBehaviour
{
	public static PlayerEventController inst = null;
	public Action<bool> OnPlayerTookDamage;

	public bool isAlive { get; set; } = true;
	private void Awake()
	{
		if (inst == null) inst = this;
	}
	public void TakeDamage()
	{
		isAlive = false;
		OnPlayerUnalive?.Invoke(false);
	}
}
