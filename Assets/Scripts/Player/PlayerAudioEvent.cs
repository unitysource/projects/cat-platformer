﻿using UnityEngine;

namespace UsingEvents
{
	[RequireComponent(typeof(AudioSource))]
	public class PlayerAudioEvent : MonoBehaviour
	{
		private AudioSource _audioSource;
		private void Awake()
		{
			_audioSource = GetComponent<AudioSource>();

			FindObjectOfType<PlayerEventController>().OnPlayerTookDamage += PlayAudioOnPlayerTookDamage;
		}

		private void PlayAudioOnPlayerTookDamage(bool isAlive)
		{
			_audioSource.Play();
		}
	}
}