﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerSaver : MonoBehaviour
{
    float timer = 0;
    [SerializeField]
    float timeLive;
    [SerializeField]
    GameObject Loading;
    void Start()
    {
        timer = 0;
    }

    void Update()
    {
        timer += Time.deltaTime;
        if(timer >= timeLive)
        {
            Loading.SetActive(true);
        }
    }
}
