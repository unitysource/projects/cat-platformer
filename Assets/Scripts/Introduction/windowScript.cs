﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class windowScript : MonoBehaviour
{
    [SerializeField]
    private float speed;
    private float finishPoz = 3.5f;
    private Vector2 startPoz;
    private float timer;
    private float mawTime = 4f;
    void Start()
    {
        timer = 0;
        startPoz = Vector2.zero;
    }

    void Update()
    {
        timer += Time.deltaTime;
        if(timer >= mawTime)
        {
            GetComponent<Animator>().SetTrigger("Disappear");
            StartCoroutine(SetActiveFalse());
        }

        transform.position += Vector3.up*speed;
        if (transform.position.y >= finishPoz)
        {
            transform.position = new Vector2(transform.position.x, finishPoz);
        }
    }

    IEnumerator SetActiveFalse()
    {
        yield return new WaitForSeconds(1);
        GetComponent<Image>().enabled = false;
    }
}
