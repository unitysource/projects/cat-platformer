﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class IntroController : MonoBehaviour
{
    [SerializeField]
    private GameObject[] windows = new GameObject[10];
    [SerializeField]
    float[] times;
    float[] timer = new float[12];
    [SerializeField]
    GameObject panepClearBlack;
    [SerializeField]
    GameObject LoadingMen;
    [SerializeField]
    GameObject SkipBtn;
    //float timeMain;
    bool startTiner;
    bool startL;
    bool finL;
    private void Start()
    {
        startTiner = true;
        startL = true;
        finL = false;
    }

    private void FixedUpdate()
    {
        if (startL)
            StartCoroutine(StartLevel());
        if (finL)
            StartCoroutine(EndIntro());
    }
    private void Update()
    {
        //timeMain += Time.deltaTime;

        //int i = 0;
        //while (i < windows.Length)
        //{
        //    if (timeMain >= timer[i])
        //    {
        //        windows[i].SetActive(true);
        //        i++;
        //    }
        //    else
        //        i++;
        //}

        if (startTiner)
            timer[0] += Time.deltaTime;
        if (timer[0] >= times[0])
        {
            SkipBtn.SetActive(true);
            startTiner = false;
            windows[0].SetActive(true);
            timer[1] += Time.deltaTime;
        }
        if (timer[1] >= times[1])
        {
            windows[1].SetActive(true);
            timer[2] += Time.deltaTime;
        }
        if (timer[2] >= times[2])
        {
            windows[2].SetActive(true);
            timer[3] += Time.deltaTime;
        }
        if (timer[3] >= times[3])
        {
            windows[3].SetActive(true);
            timer[4] += Time.deltaTime;
        }
        if (timer[4] >= times[4])
        {
            windows[4].SetActive(true);
            timer[5] += Time.deltaTime;
        }
        if (timer[5] >= times[5])
        {
            windows[5].SetActive(true);
            timer[6] += Time.deltaTime;
        }
        if (timer[6] >= times[6])
        {
            windows[6].SetActive(true);
            timer[7] += Time.deltaTime;
        }
        if (timer[7] >= times[7])
        {
            windows[7].SetActive(true);
            timer[8] += Time.deltaTime;
        }
        if (timer[8] >= times[8])
        {
            windows[8].SetActive(true);
            timer[9] += Time.deltaTime;
        }
        if (timer[9] >= times[9])
        {
            windows[9].SetActive(true);
            timer[10] += Time.deltaTime;
        }
        if (timer[10] >= times[10])
        {
            windows[10].SetActive(true);
            timer[11] += Time.deltaTime;
        }
        if (timer[11] >= times[11])
        {
            windows[10].GetComponent<Animator>().SetTrigger("Disapp");
            windows[11].SetActive(true);
            finL = true;
        }
    }

    public void Skip()
    {
        SkipBtn.GetComponent<Animator>().SetTrigger("Skip");
        finL = true;
    }

    IEnumerator EndIntro()
    {
        panepClearBlack.SetActive(true);
        yield return new WaitForSeconds(times[12]);

        panepClearBlack.GetComponent<Image>().color = new Color(panepClearBlack.GetComponent<Image>().color.r + .01f * Time.deltaTime,
            panepClearBlack.GetComponent<Image>().color.g + .01f * Time.deltaTime, panepClearBlack.GetComponent<Image>().color.b + .01f * Time.deltaTime,
            panepClearBlack.GetComponent<Image>().color.a + .5f * Time.deltaTime);
        StartCoroutine(Finish());
    }

    IEnumerator Finish()
    {
        yield return new WaitForSeconds(2f);
        LoadingMen.SetActive(true);
    }

    IEnumerator StartLevel()
    {
        panepClearBlack.SetActive(true);
        panepClearBlack.GetComponent<Image>().color = new Color(panepClearBlack.GetComponent<Image>().color.r - .01f * Time.deltaTime,
            panepClearBlack.GetComponent<Image>().color.g - .01f * Time.deltaTime, panepClearBlack.GetComponent<Image>().color.b - .01f * Time.deltaTime,
            panepClearBlack.GetComponent<Image>().color.a - .5f * Time.deltaTime);
        yield return new WaitForSeconds(1.8f);
        panepClearBlack.SetActive(false);
        startL = false;
    }
}
