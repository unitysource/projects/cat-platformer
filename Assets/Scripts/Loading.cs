﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Loading : MonoBehaviour
{
    public Image LoadingImg;
    public Text progressText;
    public bool prefer;
    void Start()
    {
        if(SceneManager.GetActiveScene().name != "Menu" & SceneManager.GetActiveScene().name != "Level_3" & SceneManager.GetActiveScene().name != "Introduction" & SceneManager.GetActiveScene().name != "Saver")
            StartCoroutine(LoadingAsync());
        if (SceneManager.GetActiveScene().name == "Level_3" || SceneManager.GetActiveScene().name == "Introduction")
            StartCoroutine(LoadingAsyncToMenu());
        if (SceneManager.GetActiveScene().name == "Saver")
            StartCoroutine(LoadingAsyncToIntro());
    }

    IEnumerator LoadingAsync()
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex + 1);
        while (!operation.isDone)
        {
            float prog = operation.progress / 0.9f; 
            LoadingImg.fillAmount = prog;
            progressText.text = string.Format("{0:0}%", prog * 100);
            yield return null;
        }
    }
    IEnumerator LoadingAsyncToMenu()
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync("Menu");
        while (!operation.isDone)
        {
            float prog = operation.progress / 0.9f;
            LoadingImg.fillAmount = prog;
            progressText.text = string.Format("{0:0}%", prog * 100);
            yield return null;
        }
    }
    IEnumerator LoadingAsyncToIntro()
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync("Introduction");
        while (!operation.isDone)
        {
            float prog = operation.progress / 0.9f;
            LoadingImg.fillAmount = prog;
            progressText.text = string.Format("{0:0}%", prog * 100);
            yield return null;
        }
    }
}
