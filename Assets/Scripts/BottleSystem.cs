﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BottleSystem : MonoBehaviour
{
    float timer;
    int count;
    public float maxTime;
    GameObject[] bottles;
    Scene curScene;
    private void Start()
    {
        count = 0;
        bottles = GameObject.FindGameObjectsWithTag("bottle");
        timer = 0;
        curScene = SceneManager.GetActiveScene();
    }

    private void Update()
    {
        timer += Time.deltaTime;

        if(timer >= maxTime)
        {
            if (count >= 4)
                count = 0;
            bottles[count].GetComponent<Image>().color = Color.Lerp(Color.white, Color.clear, .5f);
            timer = 0;
            count++;
        }

        if(bottles[0].GetComponent<Image>().color.a == 0.5 & bottles[1].GetComponent<Image>().color.a == 0.5 
            & bottles[2].GetComponent<Image>().color.a == 0.5 & bottles[3].GetComponent<Image>().color.a == 0.5)
        {
            SceneManager.LoadScene(curScene.name);
        }
    }
}
