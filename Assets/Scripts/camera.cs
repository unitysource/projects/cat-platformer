﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camera : MonoBehaviour
{
    public Transform player;
    public Transform spawnP;
    public Transform finish;
    Vector3 position;

    void Start()
    {
        transform.position = spawnP.position;
    }

    void Update()
    {
        position = player.position;
        position.z = -10f;

        transform.position = Vector3.Lerp(transform.position, position, 5f * Time.deltaTime);
        if (player.GetComponent<Player>().victory)
            transform.position = Vector3.Lerp(transform.position, finish.position, 1f);
        if (player.GetComponent<Player>().startL)
            transform.position = Vector3.Lerp(transform.position, spawnP.position, 1f);
    }
}
