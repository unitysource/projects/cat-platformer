﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicAndSounds : MonoBehaviour
{
    static int musicPassed, soundsPassed;
    [SerializeField]
    private GameObject player;
    void Start()
    {
        musicPassed = PlayerPrefs.GetInt("musicPassed");
        soundsPassed = PlayerPrefs.GetInt("soundsPassed");
        player.GetComponent<AudioSource>().volume = soundsPassed;
        gameObject.GetComponent<AudioSource>().volume = musicPassed;
    }

    void Update()
    {
        
    }
}
