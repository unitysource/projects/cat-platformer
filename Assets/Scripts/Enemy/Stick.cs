﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stick : MonoBehaviour
{
    public float speedP;
    float startSpeed;
    public float speedR;
    float timer;
    public float timeLive;
    Vector2 startP;
    void Start()
    {
        startP = new Vector2(transform.position.x, transform.position.y);
        timer = 0;
        startSpeed = speedP;
    }

    void FixedUpdate()
    {
        transform.Rotate(0, 0, speedR);
        transform.position += Vector3.left / 100 * speedP;

        timer += Time.deltaTime;
        if (timer >= timeLive)
        {
            GetComponent<Animator>().SetBool("Dead", true);
            Invoke("DeadStick", 1.3f);
        }
        if (Time.timeScale == 0)
            speedP = 0;
        else speedP = startSpeed;
    }
    void DeadStick()
    {
        transform.position = startP;
        GetComponent<Animator>().SetBool("Dead", false);
        timer = 0;
    }
}
