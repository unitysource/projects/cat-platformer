﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BushRun : MonoBehaviour
{
    public float speed;
    float startSpeed;
    public float maxTime;
    float timer;
    void Start()
    {
        timer = 0;
        startSpeed = speed;
    }

    void FixedUpdate()
    {
        float circletimer = maxTime * 2;
        timer += Time.deltaTime;
        if (timer <= maxTime)
        {
            transform.position += Vector3.left / 100 * speed;
            GetComponent<SpriteRenderer>().flipX = false;
        }
        else if (timer > maxTime & timer <= circletimer)
        {
            transform.position += Vector3.right / 100 * speed;
            GetComponent<SpriteRenderer>().flipX = true;
        }
        else
            timer = 0;
        if (Time.timeScale == 0)
            speed = 0;
        else speed = startSpeed;
    }
}
