﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class clouds : MonoBehaviour
{
    public float speed = 1f;
    Vector2 startPos;
    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        //float newPos = Time.time * speed;
        transform.position += Vector3.left / 100 * speed;
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.tag == "leftWall")
        {
            transform.position = new Vector2(75f, transform.position.y);
        }
    }
}
