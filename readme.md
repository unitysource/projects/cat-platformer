# Cat (Platformer Game)

## Table of Contents
- [About the Game](#about-the-game)
- [Gameplay](#gameplay)
- [Features](#features)
- [Getting Started](#getting-started)
- [Installation](#installation)
- [Controls](#controls)
- [Contributing](contributing)
- [License](#license)

## About the Game

Cute platformer game with cat. The game is made using Unity and C#.

## Gameplay

Basic platformer gameplay. Jump on platforms, avoid obstacles and reach the finish line.

## Features

Palette used in the game

## Getting Started

To download the game, go to the [releases page]

### Installation

1. Clone the repository: `git clone https://gitlab.com/unitysource/projects/cat-platformer`
2. Navigate to the game directory: `cd cat-platformer`
3. Install dependencies: `npm install` or `yarn install`

### Controls

Only mobile controls are available at the moment.

**Mobile Controls:**
- Swipe left/right to move.
- Tap to jump.

## Acknowledgments

- [Sound effects by SoundBible](https://www.soundbible.com/)
